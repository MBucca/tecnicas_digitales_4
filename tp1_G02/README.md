# Trabajo practico N°1
## Información
En este trabajo se implementó el bloque del correspondiente al encored, contador up/down 
de 0 a 999, la converción bcd a 7 segmento con la implementación de un par de multiplexores
el diagrama se puede ver a continuación:
![Diagrama en bloques](Diagrama en bloques.pdf)
### Pin out FPGA
![Screenshot](pin_fpga1.jpeg)
![Screenshot2](pin_fpga2.jpeg)

### Video
[Ver video](https://drive.google.com/file/d/1GLZhYUZCNE5-0OWK7gt23PEH0FJy8xy6/view?usp=sharing)
